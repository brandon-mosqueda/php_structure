<?php
require_once(MODEL_PATH."Controller.php");
require_once(MODEL_PATH."User.php");

class UserController extends Controller
{
  private $userId;
  private $name;
  private $email;

  public function __construct() 
  {
    $this->tableName = 'users';
    $this->primaryKeyName = 'userId';
  }

  public function getByUserId($userId) 
  {
    $user = self::getByPrimaryKey($userId);

    if($user === null)
      throw new Exception("User whit userId: {$userId} not found", 404);
    
    return new User($user, true);
  }

  public function create(User $user)
  {
    return self::query(
      "INSERT INTO {$this->tableName} (name, email)
      VALUES (?, ?)",
      "ss",
      [ $user->getName(), $user->getEmail() ]
    );
  }
}
?>