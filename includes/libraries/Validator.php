<?php  
require_once(LIB_PATH.'Gump.php');
require_once(EXCEPTION_PATH.'ValidatorException.php');

class Validator
{
  private $rules;
  private $hasNewOwnValidationRule;

  public function __construct($rules) 
  {
    $this->rules = $rules;
    $hasNewOwnValidationRule = false;
  }

  protected function validateOneField($field, $value) 
  {
    self::validate([ $field ], [ $value ]);
  }

  public function validate($toValidate, $fields = false) 
  {
    $toValidate = self::castIfObjectToArray($toValidate);

    if($fields === false)
      $fields = self::getKeysFromRules();

    $gump = new GUMP();
    
    $gump->sanitize($toValidate);
    $validation = [];

    foreach ($fields as $field)
      $validation[ $field ] = $this->rules[ $field ];

    $gump->validation_rules($validation);

    if(!$gump->run($toValidate)) 
    {
      //If it has new own validation, there is no error message on gump file
      //and it produces an error when calling get_readable_errors function.
      $errors = $this->hasNewOwnValidationRule ?
                "Error, incorrect params" :
                $gump->get_readable_errors(true);

      throw new ValidatorException($errors, 400);
    }
  }

  private function castIfObjectToArray($params) 
  {
    return gettype($params) === 'object' ?
           (array) $params :
           $params;
  }

  private function getKeysFromRules() 
  {
    $keys = [];

    foreach ($this->rules as $key => $value) 
      $keys[] = $key;

    return $keys;
  }

  public function addOwnValidationRule($ruleName, $ruleOptions, $callback)
  {
    $this->rules[ $ruleName ] = $ruleOptions;
    $this->hasNewOwnValidationRule = true;
    GUMP::add_validator($ruleName, $callback);
  }
}
?>