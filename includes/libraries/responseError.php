<?php
require_once('../includes/config.inc.php');

function responseError($code, $message = false, $title = false) 
{
  global $as;

  $title = $title !== false ? $title : 'Error '.$code;

  $ruta = array('<li>Error</li>');

  require_once(VIEW_PATH.'header.view.php');
  require_once(VIEW_PATH.'error.view.php');
  require_once(VIEW_PATH.'footer.view.php');

  die();
}
?>