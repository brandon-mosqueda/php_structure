<?php
function curlRequest($url, $method = 'GET', $data = null, $isJSON = true) 
{
  if($method !== 'GET' && $method !== 'POST' && $method !== 'DELETE' && $method !== 'PUT')
    throw new Exception("Method not allowed", 405);
    
  $ch = curl_init($url);

  // Configuring curl options
  $options = array(
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_CUSTOMREQUEST => $method,
    CURLOPT_HTTPHEADER => array('Content-type: application/json')
  );
  
  if($method === 'POST' || $method === 'PUT')
    if($data !== null)
      $options += $isJSON ? 
                    [CURLOPT_POSTFIELDS => json_encode($data)] : 
                    [CURLOPT_POSTFIELDS => $data];
    
  // Setting curl options
  curl_setopt_array($ch, $options);
  // Getting results
  return json_decode(curl_exec($ch)); // Getting jSON result string
}
?>