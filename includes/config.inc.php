<?php
session_start();

header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('MST');
setlocale(LC_MONETARY, 'es_ES');

if(!defined('DS'))
  define('DS', DIRECTORY_SEPARATOR);

if(!defined('SITE_ROOT'))
  define('SITE_ROOT', dirname(dirname(__FILE__)).DS);

if(!defined('INCLUDE_PATH'))
  define('INCLUDE_PATH', SITE_ROOT.'includes'.DS);

if(!defined('LIB_PATH'))
  define('LIB_PATH', INCLUDE_PATH.'libraries'.DS);

if(!defined('MODEL_PATH'))
  define('MODEL_PATH', INCLUDE_PATH.'models'.DS);

if(!defined('CONTROLLER_PATH'))
  define('CONTROLLER_PATH', INCLUDE_PATH.'controllers'.DS);

if(!defined('EXCEPTION_PATH'))
  define('EXCEPTION_PATH', INCLUDE_PATH.'exceptions'.DS);

if(!defined('VIEW_PATH'))
  define('VIEW_PATH', INCLUDE_PATH.'views'.DS);

$SERVER = json_decode(
            file_get_contents(INCLUDE_PATH.'server.json')
          )->test;

if(!defined('URL'))
  define('URL', $SERVER->url);

if(!defined('URL_CONTENT'))
  define('URL_CONTENT', URL.'content/');

if(!defined('CONTENT_PATH'))
  define('CONTENT_PATH', $SERVER->contentPath);

if(!defined('URL_CONTENT'))
  define('URL_CONTENT', $SERVER->urlContent);

if(!defined('DATABASE_HOST'))
  define('DATABASE_HOST', $SERVER->host);

if(!defined('DATABASE_NAME'))
  define('DATABASE_NAME', $SERVER->database);

if(!defined('DATABASE_USER'))
  define('DATABASE_USER', $SERVER->user);

if(!defined('DATABASE_PASSWORD'))
  define('DATABASE_PASSWORD', $SERVER->password);

unset($SERVER);

require_once(LIB_PATH.'Database.php');
require_once(MODEL_PATH.'SQL.php');

//Definidos así en la base de datos
$ROLES = (object) [
  'ADMIN' => 1
];

function isAuthenticated()
{
  return isset($_SESSION['user_id']) && $_SESSION['user_id'] !== false;
}

function isAdmin($roleId = false)
{
  global $ROLES;

  return isUserOfDeterminedRole($ROLES->ADMIN, $roleId);
}

function isUserOfDeterminedRole($roleId, $userRoleId = false) 
{
  if($userRoleId !== false)
    return intval($userRoleId) === $roleId;

  return isAuthenticated() && $_SESSION['role'] === $roleId;
}

if(isset($_SERVER['REQUEST_URI'])) 
{
  if(isset($_GET['logout']))
  {
    $_SESSION['user_id'] = false;
    session_unset();
    header("Location: ".URL);
  }
}
?>