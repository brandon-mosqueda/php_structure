<?php
require_once(MODEL_PATH . 'User.php');

class Mail 
{
  private static $messageHead = 
    '<html>
      <head>
        <title>Sistema de EJEMPLO</title>
      </head>
      <body>
        <h1 style="color: black">Universidad de Colima</h1>';

  private static $messageFoot = 
    ' </body>
      <footer>
        <br>
        <hr>
        <div style="text-align: center;"> 
          Este correo es generado automáticamente, favor de no responderlo.<br>
          Universidad de Colima
        </div>
      </footer>
    </html>';

  private static $headers = array(
    'MIME-Version: 1.0',
    'Content-type: text/html; charset=utf-8',
    'From: no-reply <sistemas@ucol.mx>'
    // 'BCC: sistemas@ucol.mx'
  );

  private static function send($message, $receiver, $subject = 'Experiencias')
  {
    $subject = '[sis-ucol] '.$subject;

    $message = self::$messageHead .$message. self::$messageFoot;

    return mail($receiver, $subject, $message, implode("\r\n", self::$headers));
  }

  public static function sendConfirmEmail($confirmURL, $userEmail)
  {
    return self::send(
      "<p>Se utilizó este correo para registrarse en el sistema de experiencias de la Universidad de Colima, para completar el registro por favor verifica tu correo entrando al siguiente enlace:</p>
        <a href=".$confirmURL.">".$confirmURL."</a>",
      $userEmail,
      "Confirma tu correo"
    );
  }

  public static function sendCreatedExperienceToValidators($isProject, $experience)
  {
    return self::send(
      "<p>Se creó ".($isProject ? 'un nuevo proyecto' : "una nueva estrategia").
        " con el nombre del programa: <strong>".$experience->program_name."</strong>, por favor visita la sección de validación de estrategias en la siguiente url para validarla</p>
        ".($isProject ? 
            "<a href=".URL."validar-proyectos/>".URL."validar-proyectos/</a>" :
            "<a href=".URL."validar-estrategias/>".URL."validar-estrategias/</a>"
          ),
        // Obtiene los correos en la forma: email1, email2, emailn, y elimina la última coma
        // Si alguna universidad no tiene validadores marcaría un error si no se le pasa otro correo
        substr(User::getValidatorsEmailByUniversityAndDiscipline($experience->university_id, $experience->disciplinary_field)->emails, 0, -1).',bmosqueda@ucol.mx',
      ($isProject ? 'Nuevo proyecto registrado' : "Nueva estrategia registrada")
    );
  }
  
  public static function sendAcceptedExperience($isProject, $experience)
  {
    return self::send(
      "<p>Se aceptó tu ".($isProject ? 'proyecto' : "estrategia").
        " con el nombre del programa: <strong>".$experience->program_name."</strong>, ahora se encontrará disponible en la sección pública o directamente en el siguiente enlace:</p>
        ".($isProject ?
            "<a href='".URL."proyecto/".$experience->id."/'>".URL."proyecto/".$experience->id."/</a>" :
            "<a href='".URL."estrategia/".$experience->id."/'>".URL."estrategia/".$experience->id."/</a>"
          ),
        User::getEmailById($experience->creator_id)->email,
      ($isProject ? 'Proyecto aceptado' : "Estrategia aceptada")
    );
  }

  public static function sendRefuseExperience($isProject, $experience, $reason)
  {
    return self::send(
      "<p>Se rechazó tu ".($isProject ? 'proyecto' : "estrategia").
        " con el nombre del programa: <strong>".$experience->program_name."</strong>, por el siguiente motivo:</p><p>".$reason."</p>",
      User::getEmailById($experience->creator_id)->email,
      ($isProject ? 'Proyecto rechazado' : "Estrategia rechazado")
    );
  }
}
?>