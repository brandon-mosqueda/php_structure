<?php
require_once(MODEL_PATH . 'SQL.php');

class Controller extends SQL 
{
  protected $tableName;
  protected $primaryKeyName;

  public function getAll() 
  {
    return self::getArrayBySql("SELECT * FROM " . $this->tableName);
  }

  public function getByPrimaryKey($id) 
  {
    return self::getObjectBySql(
      "SELECT * FROM " . $this->tableName .
      " WHERE " . $this->primaryKeyName . " = ?",
      'i',
      [$id]
    );
  }

  public function deleteById($id)
  {
    return self::query( 
      "DELETE FROM " . $this->tableName .
      " WHERE " . $this->primaryKeyName . " = ?", 
      'i', 
      [$id] 
    );
  }

  public function save()
  {
    if(self::hasPrimaryKey())
      return $this->update();
    else
      return $this->create();
  }

  private function hasPrimaryKey()
  {
    return isset($this->$primaryKeyName) && 
           $this->$primaryKeyName;
  }
}
?>