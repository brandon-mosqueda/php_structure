<?php
require_once(MODEL_PATH . "Model.php");

class User extends Model
{
  protected $userId;
  protected $name;
  protected $email;

  public function __construct($param, $comeFromDatabase = false) 
  {
    parent::__construct([
      'userId' => 'required|numeric',
      'name' => 'required|max_len,100',
      'email' => 'required|valid_email|max_len,100'
    ]);

    if(!$comeFromDatabase)
      self::validate($param, ["name", "email"]);

    $param = self::castIfArrayToObject($param);
    
    foreach ($param as $key => $value)
      $this->$key = $value;
  }

  public function getId() { return $this->id; }

  public function getName() { return $this->name; }

  public function getEmail() { return $this->email; }

  public function setUserId($userId) 
  {
    self::validateOneField('userId', $userId);
    $this->userId = $userId;
  }

  public function setName($name) 
  {
    self::validateOneField('name', $name);
    $this->name = $name;
  }

  public function setEmail($email) 
  {
    self::validateOneField('email', $email);
    $this->email = $email;
  }

  public function __toString() 
  {
    return $this->name;
  }
}
?>