<?php
abstract class SQL
{
	/**
	 * Parameterized SQL queries wrapper which receive the SQL statement and 
	 * optionally the bind param rules and array the paramethers if the 
	 * statement needs paramethers.
	 * @param  atring $sqlStament   	SQL statement in the format of my_sqli if 
	 *                                it has paramethers, e.g. param = ?.
	 * @param  string $bindParamRules Bind param rules acording whit the 
	 *                                datatypes of the paramethers, 
	 *                                e.g. 'ssi' for two strings and one integer.
	 * @param  array  $params  				The paramethers of the statement in the
	 *                             		same order that bind param's types.
	 * @return mylsqli_resultset
	 */
	public static function query(
									$sqlStament, $bindParamRules = false, $params = false)
	{
	  $database = new Database();
	  $statement = $database->stmt_init();
	  $result = null;

	  if($statement->prepare($sqlStament)) 
	  {
	    if($bindParamRules !== false)
	      $statement->bind_param($bindParamRules, ...$params);
	    
	    $result = new stdClass();
	    $result->result = $statement->execute();
	    $result->affected_rows = $statement->affected_rows;
	    $result->insert_id = $statement->insert_id;
	    $result->rows = $statement->get_result();

	    if($statement->error)
	      error_log("STATEMENT ERROR ON: ".$statement->error);

	    $statement->close();
	  }

	  if($database->error)
	    error_log("DATABASE ERROR ON: ".$database->error);

	  $database->close();

	  return $result;
	}

	public static function selectBySql($sql, $rules = false, $data = false) 
	{
		$result = self::query($sql, $rules, $data);
		
		return $result->rows;
	}
	
	public static function getArrayBySql($sql, $rules = false, $params = false) 
	{
		$resultSet = self::selectBySql($sql, $rules, $params);
	
		if($resultSet === null)
			return [];

		$array = array();
	
		while ($object = $resultSet->fetch_object())
			$array[] = $object;

		return $array;
	}

	public static function getSingleArrayBySql(
														$sql, $field, $rules = false, $params = false)
	{
		$result = self::selectBySql($sql, $rules, $params);
	
		if($result === null)
			return [];

		$array = array();
	
		while ($object = $result->fetch_object())
			$array[] = $object->$field;

		return $array;
	}

	public static function getObjectBySql($sql, $rules = false, $params = false) 
	{
		$result = self::selectBySql($sql, $rules, $params);
	
		if($result === null)
			return null;

		return $result->fetch_object();
	}

	public static function getCountBySql($sql, $rules = false, $params = false) 
	{
		$result = self::selectBySql($sql, $rules, $params);
	
		if($result === null)
			return 0;

		return intval( $result->fetch_row()[ 0 ] );
	}

	public static function getAllByTable($table) 
	{
		return self::getArrayBySql("SELECT * FROM ".$table);
	}

	public static function deleteBySql($sql, $rules = false, $params = false)
	{
		return self::query( $sql, $rules, $params );
	}
}
?>