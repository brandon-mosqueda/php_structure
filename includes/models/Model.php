<?php
require_once(LIB_PATH . 'Validator.php');

class Model
{
  private $validator;

  public function __construct($validationRules)
  {
    $this->validator = new Validator($validationRules);
  }

  protected function validate($toValidate, $fields = false)
  {
    $this->validator->validate($toValidate, $fields);
  } 

  protected function validateOneField($field, $value)
  {
    $this->validator->validateOneField($field, $value);
  }

  public function addNewValidationRule($ruleName, $ruleOptions, $callback)
  {
    $this->validator->addOwnValidationRule($ruleName, $ruleOptions, $callback);
  }

  public function toJSON ($fields = false) 
  {
    $map = new stdClass();

    if($fields === false)
      foreach ($this as $key => $value)
        $map->$key = $value;
    else
      foreach ($fields as $key) 
        $map->$key = $this->$key;

    unset($map->validator);

    return json_encode($map);
  }

  protected function castIfArrayToObject($params) 
  {
    return gettype($params) === 'array' ?
           (object) $params :
           $params;
  }
}
?>