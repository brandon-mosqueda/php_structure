 </div>
  <div class="col-md-6 col-md-offset-3 col-sm-12">
    <!-- 
      The correct answer for those sizes is <div class='col-sm-6'>. This will apply to sm and everything larger than it.
     -->
    <!-- Columns with lateral menu -->
    <!-- <div class="col-xl-9 col-lg-8 col-md-8 col-xs-12"> -->
    <!-- icons -->
      <!-- PLUS -->
        <i class="fa fa-plus"></i>

      <!-- EYE -->
        <i class="fa fa-eye"></i>

      <!-- TIMES -->
        <i class="fa fa-times"></i>

      <!-- CHECK -->
        <i class="fa fa-plus"></i>

      <!-- TRASH -->
        <i class="fa fa-trash"></i>

      <!-- Spinner -->
        <i class="fa fa-spinner fa-spin"></i>&nbsp;En progreso
    <!-- icons -->
    
    <!-- Set manual errors -->
      <div class="form-group" id="divName"><!-- Required div -->
        <label for="txtName"><b><span class="text-danger">*&nbsp;</span>Nombre</b></label>
        <input type="text" name="txtName" id="txtName" class="form-control" placeholder="Nombre" maxlength="100" required>
        <div class="form-control-feedback" id="feedName" style="display: none;"></div><!-- Required feedback -->
      </div>

      <script>
        function setError(field, error) {
          $('#div' + field).addClass('has-danger');
          $('#feed' + field).text(error);
          $('#feed' + field).show();
        }

        function hideError(field) {
          $('#div' + field).removeClass('has-danger');
          $('#feed' + field).hide();
        }
      </script>
    <!-- Set manual errors END -->


    <form id="form">
      <!-- Required field asterisk -->
        <span class="text-danger">*&nbsp;</span>
      

      <!-- Help info label -->
        <small class="form-text text-muted">Campo requerido</small>
      
      <!-- Button -->
        <div class="form-group"> <!-- Guardar -->
          <button class="form-control btn btn-success">Registrarse</button>        
        </div>
      <!-- Button END -->
      
      <!-- Classic input -->
        <div class="form-group">
          <label for="example">Nombre</label>
          <input 
            id="txtExample" 
            type="text" 
            name="example" 
            class="form-control" 
            placeholder="Example" 
            required 
            maxlength="100"
          >
        </div>
      <!-- Classic input END -->
      
      <!-- Textarea -->
        <div class="form-group">
          <label for="txtExample">EXAMPLE</label>
          <textarea 
            id="txtExample" 
            name="txtExample" 
            type="text" 
            rows="10" 
            class="form-control" 
            maxlength="3000" 
            placeholder="EXAMPLE"></textarea>
        </div>
      <!-- Textarea END -->

      <!-- Tagsinput (descomment tags input js)-->
        <div class="form-group">
          <label for="txtTags"><b><span class="text-danger">*&nbsp;</span>Palabras clave</b></label>
          <input type="text" name="txtTags" id="txtTags" class="form-control" maxlength="500" data-role="tagsinput" placeholder="Palabras clave">
        </div>
      <!-- Tagsinput END -->
  
      <!-- Checkbox -->
        <div class="form-check">
          <label for="ckbTerms"><b>Términos y condiciones</b></label><br>
          <input id="ckbTerms" name="ckbTerms" type="checkbox" class="form-check-input">
          <label class="form-check-label">Para crear este recurso es necesario que acepte nuestros
          <a href="<?php echo URL; ?>terms.php" target="_blank">términos y condiciones</a> de uso.</label>
        </div>
      <!-- Checkbox END -->

      <!-- Select multiple checkboxes and radios -->
        <style>
          fieldset {
            border: 1px solid black;
            border-radius: 5px;
            padding: 1.5em;
            border-color: rgba(0,0,0,.25); /*Borde transparente*/
          }
          legend {
            width: 85px;
          }
          .bootstrap-tagsinput {
            width: 100% !important;
          }
        </style>

        <div class="form-group" id="formObject">
          <fieldset id="fieldsetEquip">
            <legend>Equipamiento</legend>
            <div class="row">
              <div class="col-12 col-sm-6 col-md-6" id="colEquipamiento1">  </div>
              <div class="col-12 col-sm-6 col-md-6" id="colEquipamiento2">  </div>
            </div>
            <div class="row">
              <div class="col-12 col-sm-12 col-md-12">
                <label for="otherObject"><b>Otro:</b></label>
                <div class="input-group">
                  <input 
                    type="text" 
                    maxlength="70" 
                    class="form-control form-control-danger" 
                    name="otherObject" 
                    id="txtOtroObject" 
                    placeholder="Otro tipo de objeto"
                  >
                  <span class="input-group-btn">
                    <button class="btn btn-success" id="btnOherObject" type="button">Agregar</button>
                  </span><br>
                </div>
                <div class="form-control-feedback" hidden id="feedbackEquip"></div>
              </div>
            </div>
          </fieldset>
          <span class="form-text text-muted"><i>&nbsp;Ingresar las herramientas con las que cuenta el espacio</i></span><br>
          <div class="form-group" id="divEquipamiento">
            <div class="form-control-feedback" id="feedEquipamiento" style="display: none;"></div>
          </div>
        </div>
        <script>
          function createCheck(name, val, classType, checked = false) {
            return '<div class="form-check">'+
                      '<input type="checkbox" '+(checked ? 'checked' : '')+' name="'+name+'" value="'+val+'" class="'+classType+'">'+
                      '<label for="'+name+'">&nbsp;'+name+'</label>' +
                   '</div>'
          }

          for (var i = 0; i < data.length; i++) {
            var divTemp = createCheck(data[i].name, data[i].id, "classNAME");
            i % 2 == 0 ? $('#colEquipamiento1').append(divTemp) : $('#colEquipamiento2').append(divTemp);
          }

          //Validar que por lo menos uno está seleccionado
          $("input[class='discipline']:checked").val();

          if($("input[class='discipline']:checked").val() === undefined) {
            window.location = '#formDisciplines'  
            return false;
          }
          return true;

          function getSelectedCatalogosByClass(className) {
            var result = [];
            $.each($('.' + className), function(index, val) {
              if(val.checked) {
                var temp = { catalogId: val.value};
                result.push(temp);
              }
            }); 

            return result;
          }
        </script>
      <!-- Select multiple checkboxes and radios END -->

      <!-- Select -->
        <div class="form-group">
          <label for="selectOPTION"><b>OPTION</b></label>
          <select name="selectOPTION" class="form-control" id="selectOPTION" aria-invalid="false">
          </select>
        </div>
      <!-- Select END -->
      
      <!-- Tables -->
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Archivo / URL</th>
              </tr>
            </thead>
            <tbody id="fileTable">
            </tbody>
          </table>
        </div>
      <!-- Tables END  -->

      <!-- Files -->
        <div class="form-group"><!-- File -->
            <label for="fileInput"><span class="text-danger">*&nbsp;</span><b>Documento / Archivo</b></label>
            <input id="fileInput" name="fileInput" class="form-control" accept=".pdf, .docx, .doc" type="file" required>
            <small class="form-text text-muted">Sólo PDF's y documentos de Word</small>
        </div>
      <!-- Files END -->

      <!-- IMAGES -->
        <div class="form-group" id="divImagen">
          <label for="image">Image de portada</label>
          <div class="form-control-feedback" id="feedImage" style="display: none;"></div>
          <input 
            type="file" 
            class="form-control" 
            name="image" 
            id="inputImagen" 
            accept="image/*" 
            required
          >
          <img 
            src="default.png" 
            style="display:block;margin:auto;" 
            class="img-fluid" 
            id="inputImage" 
            alt="Responsive image" 
          >
          <span class="form-text text-muted"><i>&nbsp;Colocar una fotografía representativa del espacio ofertado</i></span><br>       
        </div>
      <!-- IMAGES END -->
    </form>
  </div>
<script>
  //Ocultar la barra lateral para que el formulario ocupe toda la pantalla
  $('#sidebar').hide();
</script>
<?php require_once(VIEW_PATH.'modal.view.php'); ?>
<script src="<?php echo URL; ?>public/js/http.js"></script>
<script src="<?php echo URL; ?>public/js/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
<!-- <script src="<?php echo URL; ?>public/js/bootstrap-tagsinput.min.js"></script> -->
<script src="<?php echo URL; ?>public/js/EXAMPLE.js"></script>