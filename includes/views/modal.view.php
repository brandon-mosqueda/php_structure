<!-- Modal section -->
  <div class="modal" tabindex="-1" role="dialog" id="modalInfo">
    <div class="modal-dialog" role="document">
      <div class="modal-content" id="modalBody">
        <div class="modal-header">
          <h5 class="modal-title" id="modalInfoTitle"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-12">
              <div class="form-group">
                <div id="modalInfoContent"></div>
                <button 
                  type="button" 
                  class="btn btn-primary pull-right" 
                  data-dismiss="modal"
                >Aceptar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
    var modal = {
      alert: null,
      refuse: null,
      confirm: null
    };

    (function() {
      // Info: 1, Confirm: 2, Refuse: 3
      var modalStatus = 0;

      var modalInfoTemplate = 
        '<div class="modal-header">' +
          '<h5 class="modal-title" id="modalInfoTitle"></h5>' +
          '<button id="btnTimes" type="button" class="close" data-dismiss="modal" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
          '</button>' +
        '</div>' +
        '<div class="modal-body">' +
          '<div class="row">' +
            '<div class="col-md-10 col-md-offset-1 col-sm-12 col-12">' +
              '<div class="form-group">' +
                '<div id="modalInfoContent"></div>' +
                '<button ' +
                  'type="button" ' +
                  'class="btn btn-primary pull-right" ' +
                  'id="btnAccept"' +
                '>Aceptar</button>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>';

      var modalConfirmTemplate = 
        '<div class="modal-header">' +
          '<h5 class="modal-title" id="modalInfoTitle">Confirmación</h5>' +
          '<button type="button" id="btnTimes" class="close" data-dismiss="modal" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
          '</button>' +
        '</div>' +
        '<div class="modal-body">' +
          '<div class="row">' +
            '<div class="col-md-10 col-md-offset-1 col-sm-12 col-12">' +
              '<div class="form-group">' +
                '<div id="modalInfoContent"></div>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>'+
        '<div class="modal-footer">'+
          '<button type="button" class="btn btn-success" id="btnConfirm">'+
            'Aceptar</button>'+
          '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>'+
        '</div>';

      var modalRefuseTamplate = 
        '<div class="modal-header">'+
          '<h5 class="modal-title">Motivo de rechazo'+
          '</h5>'+
          '<button id="btnTimes" type="button" class="close" data-dismiss="modal" aria-label="Close">'+
            '<span aria-hidden="true">×</span>'+
          '</button>'+
        '</div>'+
        '<div class="modal-body">'+
          '<div class="form-group">'+
            '<label for="txtReason">Motivo</label>'+
            '<textarea id="txtReason" name="txtReason" type="text" rows="10" class="form-control"'+
            'maxlength="2000" placeholder="Motivo"></textarea>'+
            '<label id="txtReasonError" class="error" for="txtReason" style="display: none;">'+
              'Ingresa el motivo'+
            '</label>'+
          '</div>'+
          '<label id="lblReasonError" class="error" style="display: none;">Ingresa el motivo</label>'+
        '</div>'+
        '<div class="modal-footer">'+
          '<button type="button" class="btn btn-danger" id="btnRefuse" value="0">'+
            'Deshabilitar</button>'+
          '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>'+
        '</div>';

      modal.alert = function(param) {
        if(typeof param !== 'object')
          throw 'Las opciones deben ser un objeto';

        var options = {
          title: param.title || 'Información',
          content: param.content || 'Hola mundo',
          isError: typeof param.isError === 'boolean' ? param.isError : false,
          callback: typeof param.callback === 'function' ? param.callback: undefined,
          isCloseable: typeof param.isCloseable === 'boolean' ? param.isCloseable : true
        };

        if(modalStatus !== 1) 
          $('#modalBody').html(modalInfoTemplate);

        modalStatus = 1;

        $('#modalInfoTitle').html(
          (options.isError !== false ?
            '<span class="text-danger">'+
              '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;&nbsp;'+
            '</span>':
            '<span class="text-info">'+
              '<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;'+
            '</span>' 
          ) + options.title
        );

        $('#modalInfoContent').html(options.content);

        $('#btnAccept').unbind('click');

        $('#btnAccept').click(function() {
          $('#modalInfo').modal('hide');
          
          if(options.callback !== undefined)
            options.callback();
        });  

        if(options.isCloseable)
          $("#modalInfo").modal();
        else {
          $("#modalInfo").modal({backdrop: 'static', keyboard: false});
          $('#btnTimes').hide();
        }
      }

      modal.refuse = function(param) {
        if(typeof param !== 'object')
          throw 'Las opciones deben ser un objeto';

        if(typeof param.callback !== 'function')
          throw 'El callback es necesario';

        var options = {
          title: param.title || 'Rechazar',
          callback: param.callback,
          isCloseable: typeof param.isCloseable === 'boolean' ? param.isCloseable : true
        };

        if(modalStatus !== 3) 
          $('#modalBody').html(modalRefuseTamplate);

        modalStatus = 3;

        $('#txtReason').val('');
        $('#btnConfirm').unbind('click');
        
        $('#btnRefuse').unbind('click');
        
        $('#btnRefuse').click(function() {
          var reason = $('#txtReason').val().trim();
          if(reason === '') {
            $('#lblReasonError').show();
            return;
          }
          $('#lblReasonError').hide();

          options.callback(reason);
          $('#modalInfo').modal('hide');
        });

        if(options.isCloseable)
          $("#modalInfo").modal();
        else {
          $("#modalInfo").modal({backdrop: 'static', keyboard: false});
          $('#btnTimes').hide();
        }
      }

      modal.confirm = function(param) {
        if(typeof param !== 'object')
          throw 'Las opciones deben ser un objeto';

        if(typeof param.callback !== 'function')
          throw 'El callback es necesario';

        if(typeof param.content !== 'string' || param.content.length < 0)
          throw 'El contenido es necesario';

        var options = {
          callback: param.callback,
          content: param.content,
          isCloseable: typeof param.isCloseable === 'boolean' ? param.isCloseable : true
        };

        if(modalStatus !== 2) 
          $('#modalBody').html(modalConfirmTemplate);
        
        modalStatus = 2;

        $('#modalInfoContent').html(param.content);

        $('#btnConfirm').unbind('click');
        
        $('#btnConfirm').click(function() {
          param.callback(true);
          $('#modalInfo').modal('hide');
        });

        if(options.isCloseable)
          $("#modalInfo").modal();
        else {
          $("#modalInfo").modal({backdrop: 'static', keyboard: false});
          $('#btnTimes').hide();
        }
      }
    })();
  </script>
<!-- Modal section END -->