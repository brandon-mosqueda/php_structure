<?php 
  echo file_get_contents("https://www.ucol.mx/cms/headerapp2.php?TITULO=".urlencode("Actividades")); 
?>
<link 
  href="<?php echo URL.'public/css/dataTables.bootstrap4.min.css'; ?>" 
  rel="stylesheet" 
  type="text/css"
></link>
<script src="<?php echo URL.'public/js/jquery.dataTables.min.js'; ?>"></script>
<script src="<?php echo URL.'public/js/dataTables.bootstrap4.min.js'; ?>"></script>
<script src="https://www.ucol.mx/cms/beta/dist/js/bootstrap.min.js"></script>
<style>
  label.error {
    font-weight: bold !important;
    color: #d9534f !important;
  }
	small.form-text.text-muted {
		display: block;
	}
</style>
<section class="page-breadcrumb">
	<div class="container ">
		<div id="path">
      <ol class="breadcrumb">
        <li>Usted est&aacute; en:</li>
        <li><a href="<?php echo URL; ?>">Inicio</a></li>
        <?php  
        	for ($i=0; $i < count($ruta); $i++)
        		echo '<li>'.$ruta[$i].'</li>';//En el archivo .php que manda llamar al header se declara como array
        ?>
			</ol>
	  </div>
	  <div id="sesion">
      <ol class="breadcrumb">
	      <?php if($as->isAuthenticated()): ?> <!--Validar sesión -->
		      <li>Bienvenido(a):</li>
	        <li class="user-name"><?php echo $_SESSION['name']; ?></li>
	        <li><a id="linkLogout" href="?logout">Salir</a></li>
	      <?php else ?>
          <li><a id="linkLogin" href="?login">Iniciar sesión</a></li>;
        <?php endif;?>
			</ol>
	  </div>
	</div>
</section>
<section class="page-header">
  <div class="container">
    <h1 class="title-ucol" id="lblTitulo"><?php echo $title; ?></h1>
  </div>
</section>
<script>
  var gURL = '<?php echo URL; ?>';
</script>
<div class="container c-principal">
  <div class="row p-contenido">
      <div class="col-xl-3 col-lg-4 col-md-4 col-xs-12 sidebar" id='sidebar'>
			<h2>Men&uacute;</h2>
         <div id="navcontainer">
            <ul id="navlist">
               <li>
                  Mi Perfil
                  <ul id="navlist">
                     <li><a href="<?php echo URL; ?>mis-actividades">Mis actividades</a></li>
                     <li><a href="<?php echo URL; ?>crear-actividad">Crear actividad</a></li>
                     <li><a href="<?php echo URL; ?>mis-solicitudes">Mis solicitudes</a></li>
                  </ul>
               </li>
               <?php if ($as->isAuthenticated() && $_SESSION['role'] === 1): ?>
                <li>
                    Administar
                    <ul id="navlist">
                      <li><a href="<?php echo URL; ?>admin">Actividades</a></li>
                      <li><a href="<?php echo URL; ?>eventos">Catálogo de eventos</a></li>
                    </ul>
                </li>
              <?php endif; ?>
            </ul>
         </div>