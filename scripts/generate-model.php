<?php  
/*
  String: s
  Boolean: b
  Integer: i
  Double: double
*/
header('Content-Type:text/plain'); 
//In title case, such as: User
$model = 'Test';
$primaryKey = 'test_id';

//In lower case
$props = (object) [
  "test_id" => 'i',
  "name" => 's',
  "email" => 's'
];

function generateModel($model, $props, $pk)
{
  defined('TAB') ? NULL : define('TAB', '  ');

  //private $id;
  $classAttrs = '';

  //'id' => 'required|numeric',
  $attrsValRules = '';

  //'name', 'email'
  $arrayFields = '';

  //name, email, para el query de insert
  $createFields = '';

  //Interrogation ?, to create and update method
  $interrogation = '';

  //Bind params 'sssiiss'
  $bindParamsLetters = '';
  $bindParams = '';

  //To update method SET name = ?
  $setParams = '';

  //public function getName() { return \$this->name; }
  $getters = '';

  // public function setName(\$name) 
  // {
  //   self::validateOneField('name', \$name);
  //   \$this->name = \$name;
  // }
  $setters = '';

  function getTabs($n) 
  {
    $tab = '';
    for ($i = 0; $i < $n; $i++) 
      $tab .= TAB;

    return $tab;
  }

  function toTitleCase($str) 
  {
    $str[0] = strtoupper($str[0]);

    return $str;
  }

  //Recibe un una cadena y la devuelve en camel case, si tiene _ se quita
  function getSetName($name) 
  {
    $new = strtoupper($name[0]);

    for ($i = 1; $i < strlen($name); $i++) 
    {
      if($name[ $i ] !== '_')
        $new .= $name[ $i ];
      else
        $new .= strtoupper($name[ ++$i ]);
    }

    return $new;
  }

  function getRules($att)
  {
    switch ($att) 
    {
      case 'i':
        return "required|integer";
      case 'b':
        return "required|boolean";
      case 'd':
        return "required|numeric";
      default:
        return "required|max_len,100|min_len,10";
    }
  }

  foreach ($props as $key => $val)
  {
    $classAttrs .= "protected $".$key.";\n".getTabs(1);
    $attrsValRules .= "'".$key."' => '".getRules($val)."',\n".getTabs(3);
    $getters .= "public function get".getSetName($key)."(){ return \$this->".$key."; }\n\n".getTabs(2);
    $setters .= "public function set".getSetName($key)."(\$".$key.")\n".getTabs(2).
        "{\n".getTabs(3).
          "self::validateOneField('".$key."', \$".$key.");\n".getTabs(3).
          "\$this->".$key." = \$".$key.";
    }\n\n".getTabs(2);

    $arrayFields .= "'".$key."',\n".getTabs(5);
    $createFields .= $key.",\n".getTabs(7);
    $interrogation .= "?, ";
    $bindParamsLetters .= $val === 'b' ? 'i' : $val;
    $bindParams .= "\$this->".$key.",\n".getTabs(4);    
    $setParams .= $key." = ?,\n".getTabs(8);
  }
  //Quitar el último TAB 
  $attrsValRules = substr($attrsValRules, 0, -8);
  $getters = substr($getters, 0, -6);
  $setters = substr($setters, 0, -6);

  $arrayFields = substr($arrayFields, 0, -2);
  $interrogation = substr($interrogation, 0, -2);
  $bindParams = substr($bindParams, 0, -10);
  $arrayFields = substr($arrayFields, 0, -10);
  $createFields = substr($createFields, 0, -16);
  $setParams = substr($setParams, 0, -18);

  $result = "<?php
require_once(LIB_PATH.'gump.class.php');

class ".$model." extends General
{
  ".$classAttrs."
  public function __construct(\$param) 
  {
    \$this->validationFields =  [
      ".$attrsValRules."
    ];

    if(is_numeric(\$param)) 
      \$param = self::getById(\$param);
    else if(gettype(\$param) === 'object') 
    {
      self::validate(
        [
          ".$arrayFields."
        ], 
        \$param
      );

      \$this->".$pk." = false;
    }
    else
      throw new Exception('Constructor de ".strtolower($model).", parámetros incorrectos', 400);

    if(\$param !== null) 
      foreach (\$param as \$field => \$value)
        \$this->\$field = \$value;
    else
      throw new Exception('".strtolower($model)." no encontrado', 404);
  }
  
  public function create()
  {
    \$sql = \"INSERT INTO ".strtolower($model)."s
            (
              ".$createFields."
            ) 
            VALUES (".$interrogation.")\";
    
    \$result = self::query(
      \$sql, 
      '".$bindParamsLetters."', 
      [
        ".$bindParams."
      ]
    );

    if(!\$result->result)
      return false;

    \$this->".$pk." = \$result->insert_id;
    return true;
  }

  public function update()
  {
    \$sql = \"UPDATE ".strtolower($model)."s  
            SET ".$setParams." 
            WHERE ".$pk." = ?\";
    
    \$result = self::query(
      \$sql, 
      '".$bindParamsLetters."i', 
      [
        ".$bindParams.",
        \$this->".$pk."
      ]
    );

    if(!\$result->result)
      return false;

    return \$result->affected_rows > 0;
  }
  
  // SELECT

  //SELECT END

  //Override functions
    //Para que se pueda usar dentro de cualquier otro método usar static::method
    protected static function getTableName() { return '".strtolower($model)."s'; }
    
    //Para el caso de que la llave primaria se llame model_id
    protected static function getPrimaryKeyName() { return '".$pk."'; }
  //Override functions END

  // GETTERS
    ".$getters."
  // GETTERS END

  // SETTERS
    ".$setters."
  // SETTERS END
}
?>";

  return $result;
}

echo generateModel($model, $props, $primaryKey);
?>
