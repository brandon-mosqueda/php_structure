<?php  

header('Content-Type:text/plain'); 
//In title case, such as: User
$model = 'Usuario';

//Model on camelcase
function generateAPI($model) 
{
  defined('TAB') ? NULL : define('TAB', '  ');

  $lowerModel = strtolower($model);

  $result = "<?php
    require_once('../includes/config.inc.php');
    require_once(MODEL_PATH.'".$lowerModel.".model.php');
    require_once(LIB_PATH.'responseRequest.php');

    //Validar que está loggueado, si no requiere inicio de sesión poner en api/public
    if(!\$as->isAuthenticated())
      responseRequest(407, 'Autenticación requerida', true);

    header('Content-Type: application/json');

    switch (\$_SERVER['REQUEST_METHOD']) 
    {
      case 'GET':
        if (!count(\$_GET)) 
          echo ".$model."::getAll('".$lowerModel."s');
        else if(isset(\$_GET['id']) && is_numeric(\$_GET['id']))
          echo ".$model."::getById(\$_GET['id'], '".$lowerModel."s');
        else
          responseRequest(400, 'Parámetros no válidos');
        break;

      case 'POST':
        \$body = json_decode(file_get_contents('php://input'));
        if(\$body === null)
          responseRequest(400, 'No se recibió body', true);
          
        if (!count(\$_GET)) 
        {
          try {
            \$".$lowerModel." = new ".$model."(\$body);

            if(\$".$lowerModel."->create()) 
            { 
              http_response_code(201);
              echo \$".$lowerModel."->toJSON();
            }
            else
              responseRequest(500, 'Hubo un problema al guardar el ".$lowerModel."');

          } catch (Exception \$ex) {
            responseRequest(\$ex->getCode(), \$ex->getMessage());
          }
        }
        else
          responseRequest(400, 'Parámetros no válidos');
        break;

      case 'PUT':
        \$id = isset(\$_GET['id']) && is_numeric(\$_GET['id']) ? intval(\$_GET['id']) : false;
        \$body = json_decode(file_get_contents('php://input'));

        if (count(\$_GET) === 1) 
        {
          try {
            \$".$lowerModel." = new ".$model."(\$id);

            if(\$".$lowerModel."->update()) 
            { 
              http_response_code(200);
              echo \$".$lowerModel."->toJSON();
            }
            else
              responseRequest(500, 'Hubo un problema al actualizar el ".$lowerModel."');

          } catch (Exception \$ex) {
            responseRequest(\$ex->getCode(), \$ex->getMessage());
          }
        }
        else if(isset(\$_GET['status']) && is_numeric(\$_GET['status']))
        {
          //Guard: -2, Envi: -1, Can = 0, Apro = 1
          if(\$_GET['status'] == 0)
            if(!(isset(\$body->reason) && strlen(\$body->reason) > 0))  //Motivo es requerido
              responseRequest(400, 'Es necesario el motivo', true);

          try {
            \$".$lowerModel." = new ".$model."(\$id);

            \$".$lowerModel."->setStatus(\$_GET['status']);

            if(\$".$lowerModel."->update())
            {
              \$_GET['status'] == 0 ? 
                  Mail::refuseMail(\$body->reason) : 
                  Mail::refuseMail();
              http_response_code(204);   //No content
            }
            else
              responseRequest(500, 'Hubo un problema al actualizar el estatus');

          } catch (Exception \$ex) {
            responseRequest(\$ex->getCode(), \$ex->getMessage());
          }
        }
        else
          responseRequest(400, 'Parámetros no válidos');
        break;

      case 'DELETE':
        \$id = isset(\$_GET['id']) && is_numeric(\$_GET['id']) ? intval(\$_GET['id']) : false;

        if (count(\$_GET) === 1 && $id) 
        {
          try {
            \$".$lowerModel." = new ".$model."(\$id);
            if(\$".$lowerModel."->deleteById(\$id)) 
            { 
              http_response_code(200);
              echo \$".$lowerModel."->toJSON();
            }
            else
              responseRequest(500, 'Hubo un problema al borrar el ".$lowerModel."');

          } catch (Exception \$ex) {
            responseRequest(\$ex->getCode(), \$ex->getMessage(), true);
          }
        }
        else
          responseRequest(400, 'Parámetros no válidos');
        break;

      default:
        responseRequest(405, 'Método de HTTP no permitido');
    }
    ?>";

  return $result;
}

echo generateAPI($model);
?>