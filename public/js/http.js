var http = function() {
  var header = "application/json";

  function setHeader (h) {
    header = h;
  }

  function getHeader() {
    return header;
  }
  
  function get(url, callback) {
    __request('GET', url, null, callback);
  }
   
  function post(url, params, callback) {
    if(typeof params === 'function') {
      callback = params;
      params = null;
    }
    
    __request('POST', url, params, callback);
  }

  function put(url, params, callback) {
    if(typeof params === 'function') {
      callback = params;
      params = null;
    }
    
    __request('PUT', url, params, callback);
  }

  function del(url, callback) {
    __request('DELETE', url, null, callback);
  }

  function __request(method, url, params, callback) {
    var req = new XMLHttpRequest();

    req.open(method, url, true);
    if (header === 'application/json')
      req.setRequestHeader("Content-type", header);

    req.onerror = function() {
      try {
        callback(JSON.parse(req.responseText), undefined, req.status);
      } catch (err) {
        callback(req.responseText, undefined, req.status);
      }
    }

    req.onload = function() {
      if(req.readyState == 4 && req.status >= 200 && req.status <= 300) {
        try {
          callback(undefined, JSON.parse(req.responseText), req.status);
        } catch(err) {
          callback(undefined, {message: "Todo correcto, no content"}, req.status);
        }
      }
      else {
        try {
          callback(JSON.parse(req.responseText), undefined, req.status);
        } catch (err) {
          callback(req.responseText, undefined, req.status);
        }
      }
    }

    //La función send valida internamente que sea una petición que acepte parámetros, de lo contrario lo setea a null
    if (header === 'application/json')
      req.send(JSON.stringify(params));
    else
      req.send(params);
  }

  return {
    setHeader: setHeader,
    getHeader: getHeader,
    get: get,
    post: post,
    put: put,
    delete: del
  };
}();