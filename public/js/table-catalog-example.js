var table = {};
var objects = [];
var tempRow = {};

/*HTTP requests*/
  function getObjects(callback) {
    http.get(gURL + 'api/catalog.php', function(error, data, status) {
      if(error !== undefined) {
        showModal(
          'Error al obtener catálogos:<br>' + data,
          'Error ' + status,
          true
        );

        return;
      }
      objects = data;

      callback();
    }); 
  }
/*HTTP requests END*/

/*HTML templates*/
  function createOptionsButtons(id, index, status) {
    //Guar: - 2, Envi: -1 , Apro = 1; Can = 0
    status = Number(status);
    var btnRefuse = status === 1 || status === -1 ?
                  '<button value="'+index+'" class="btn btn-small btn-danger refuse">' +
                    '<i class="fa fa-times" aria-hidden="true"></i>'+
                    '<span>&nbsp;Deshabilitar</span>' +
                  '</button>' : '';

    var btnAccept = status === 0 || status === -1 ?
                  '<button value="'+index+'" class="btn btn-small btn-success accept">' +
                    '<i class="fa fa-check" aria-hidden="true"></i>'+
                    '<span>&nbsp;Habilitar</span>' +
                  '</button>' : '';

    return '<div class="btn-group-vertical btn-group-sm">'+
              btnRefuse +
              btnAccept +
              '<button value="'+index+'" class="btn btn-small btn-primary profile">' +
                '<i class="fa fa-user" aria-hidden="true"></i>'+
                '<span>&nbsp;Ver perfil</span>' +
              '</button>' +
            '</div>';
  }

  function createStatusLabel(status) {
    //Envi: -1 , Apro = 1; Can = 0
    status = Number(status);
    var lblStatus = '<i class="fa fa-spinner" aria-hidden="true"></i>&nbsp;<span>En espera</span>';

    if(status === 0)
      lblStatus = '<i class="fa fa-times-circle text-danger" aria-hidden="true"></i>&nbsp;<span>Deshabilitado</span>';
    else if(status === 1)
      lblStatus = '<i class="fa fa-check-circle text-success" aria-hidden="true"></i>&nbsp;<span>Habilitado</span>';

    return lblStatus;
  }

  // Necessary class
  // .label-dark {
  //   background-color: #343a40;
  //   border-color: #343a40;
  // }
  function createSpansTags(tags) {
    var tagsArray = tags.split(',');
    var strTags = '';

    for (var i = tagsArray.length - 1; i >= 0; i--)
      strTags += '<span class="label label-dark static-tag" style="font-size: 80% !important;">'+tagsArray[i]+'</span>&nbsp;';

    return strTags;
  }
/*HTML templates END*/

/*Events*/
  $(window).load(() => {
    table = $('#table').DataTable({
      "iDisplayLength": 10,
      "lengthMenu": [[10, 25, 50], [10, 25, 50]],
      "order": [],
      "language": {
        "lengthMenu": "_MENU_ Registros por p&aacute;gina",
        "zeroRecords": "Registros no encontrados",
        "info": "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
        "infoEmpty": "Registros no disponibles",
        "infoFiltered": "(filtrado de _MAX_ registros totales)",
        "sSearch": "Buscar",
          "oPaginate": {  
            "sLast": "Final",
            "sNext": "Siguiente", 
            "sFirst": "Principio",
            "sPrevious": "Anterior"
          }
      } 
    });

    getObjects(function(){
      //Cargar en la tabla todos los registros
      for (var i = objects.length - 1; i >= 0; i--) 
        table.row.add(objectsToArray(objects[ i ], i));

      table.draw();

      $('tbody').on('click', 'button.refuse', showModalDisable);
      $('tbody').on('click', 'button.accept', enableProfessor);
    });
  });

  function refuseObjects() {
    var index = $(this).val();
    tempRow = $(this).parents('tr');

    showModalRefuse(function(reason) {
      http.put(gURL+'api/objects.php?status=0&id='+objects[index].id, {reason: reason}, function(error, data, status) {
        if(error !== undefined) {
          showModal(
            'Error al rechazar objeto:<br>' + error.message,
            'Error ' + status,
            true
          );

          return;
        }

        objects[index] = null;
        table.row(tempRow).remove().draw();
      });
    }):
  }

  function acceptObjects() {
    var index = $(this).val();

    http.put(gURL+'api/objects.php?status=1&id='+objects[index].id, function(error, data, status) {
      if(error !== undefined) {
        showModal(
          'Error al aceptar objeto:<br>' + error.message,
          'Error ' + status,
          true
        );

        return;
      }

      objects[index].status = 1;
      table.row(tempRow).data(objectsToArray(objects[index], index));
      table.draw(true);
    });
  }
/*Events END*/

function objectsToArray(object, index) {
  return [
    object.name,
    universities[object.university_id],
    object.email,
    createStatusLabel(object.status),
    createOptionsButtons(object.id, index, object.status)
  ];
}