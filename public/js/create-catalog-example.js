var disciplines = [];
var universities = [];

/*http requests*/
  function getUniversities(callback) {
    http.get(url + 'api/university.php', function(error, data, status) {
      if(error !== undefined) {
        console.error(error);
        console.error(status);
        alert('Hubo un problema al cargar las universidades y se recargará la página');
        setTimeout(function(){
          window.reload();
        }, 4000);

        return;
      }
      universities = data;
      callback(data);
    }); 
  }
/*http requests END*/

/*Events*/
  $(window).load(function() {
    getDisciplines(function(){
      for (var i = disciplines.length - 1; i >= 0; i--) {
        $('#selectDiscipline').append('<option '+
          'value="'+disciplines[i].id+'">'+disciplines[i].name+
          '</option>');
      }
    });

    getUniversities(function(){
      for (var i = universities.length - 1; i >= 0; i--) {
        $('#selectUniversity').append('<option '+
          'value="'+universities[i].id+'">'+universities[i].name+
          '</option>');
      }
    });
  });
/*Events END*/

/*Validaciones*/
  //********* NOTA: El input debe tener el mismo id y nombre para que funcione
  jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
      // phone_number = phone_number.replace(/\s+/g, "");
      if(phone_number === '')
        return true;
      return phone_number.match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/);
  }, "Por favor ingresa un número de teléfono válido");

  $('#formEXAMPlE').validate({
    ignore: "not:hidden", //Para que también valide en inputs hidden
    lan: 'es',
    rules: {
      txtPassword: {
        required: true,
        maxlength: 32,
        minlength: 8,
        equalTo: "#txtPassword"
      }
    },
    messages: {
      txtPassword: {
        required: "La contraseña es requerida",
        minlength: "La contraseña debe de tener por lo menos 8 caracteres",
        maxlength: "La contraseña no puede tener más de 32 caracteres",
        equalTo: "Las contraseñas no coinciden"
      }
    },
    submitHandler: function(form, event) {
      event.preventDefault();

      var email = $('#txtEmail').val();

      var user = {
        name: $('#txtName').val()
      }

      http.post(gURL + 'api/EXAMPLE.php', user, function(error, data, status) {
        if(error !== undefined) {
          showModal(
            'Error al crear objeto:<br>' + data,
            'Error ' + status,
            true
          );

          return;
        }

        showModal('Objeto creado exitosamente', 'Creación exitosa');
      });
    }
  });
/*Validaciones END*/

function getRandom() { return Math.round(Math.random() * 100); }

function test() {
  var random = getRandom();

  $('#txtName').val('Hola' + random);
  $('#txtPlantel').val('Plantel chido' + random);
}