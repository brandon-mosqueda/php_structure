<?php  
require_once('includes/config.inc.php');
require_once(CONTROLLER_PATH . 'UserController.class.php');

$userController = new UserController();

try {
  // $user = new User((object) ["name" => "Brandon", "email" => "mosqueda@google.com"]);
  $user = $userController->getByUserId($_GET['userId']);
  
  // var_dump($userController->create($user)->affected_rows);

  echo "<pre>";
  echo $user->toJSON();
  echo "</pre>";
} catch(Exception $ex) {
  var_dump($ex->getMessage());
}
?>